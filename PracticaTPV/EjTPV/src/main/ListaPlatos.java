package main;

import java.util.ArrayList;

public class ListaPlatos {
		
	private ArrayList<Plato> listaPlatos;
	
	
	public ListaPlatos(){
		
		
		listaPlatos= new ArrayList<Plato>();
		
		Plato plato = new Plato("Botella de agua",(float)1.5);
		listaPlatos.add(plato);
		
		plato = new Plato("Botella de coca-cola",(float)2.0);
		listaPlatos.add(plato);
		
		plato = new Plato("Plato empanadillas",(float)15);
		listaPlatos.add(plato);
		
		
		plato = new Plato("Lata naranajada",(float)1.5);
		listaPlatos.add(plato);
		
		plato = new Plato("Plato longaniza",(float)10);
		listaPlatos.add(plato);
		
		plato = new Plato("Patatas fritas ",(float)10);
		listaPlatos.add(plato);
		
		plato = new Plato("Plato pescado frito",(float)10);
		listaPlatos.add(plato);
		
		plato = new Plato("Bocadillo jamon",(float)6);
		listaPlatos.add(plato);
		
		plato = new Plato("Plato croquetas",(float)8);
		listaPlatos.add(plato);
		
		plato = new Plato("Taza cafe con leche",(float)2);
		listaPlatos.add(plato);
		
		plato = new Plato("Plato chipirones",(float)12.5);
		listaPlatos.add(plato);
		
		plato = new Plato("Plato calamares",(float)8);
		listaPlatos.add(plato);
		
		
	}
	
	public ArrayList<Plato> darListaPlatos(){
		
		return listaPlatos;
		
	}
	

}
