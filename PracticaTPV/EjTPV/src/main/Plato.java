package main;

public class Plato {
	
	
	private String nombre;
	private float precioUnidad;
	private int cantidad;
	private float total;
	private int posicionPedido;
	
	public Plato(String nombre, float precioUnidad){
		this.nombre=nombre;
		this.precioUnidad=precioUnidad;
		this.cantidad=0;
		this.total=(float)0.0;
		this.posicionPedido=0;
	}

	
	public Plato(){
		
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float subtotal) {
		this.total = subtotal;
	}


	public int getPosicionPedido() {
		return posicionPedido;
	}


	public void setPosicionPedido(int posicionPedido) {
		this.posicionPedido = posicionPedido;
	}


	@Override
	public String toString() {
		return posicionPedido+"\t"+nombre+"  \t"+precioUnidad+"�\t"+cantidad+"\t"+total;
	}

	
	
	
	
	
	
	
}
