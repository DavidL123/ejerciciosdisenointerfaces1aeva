package main;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {


		Marco m = new Marco();
		
		m.setResizable(false);
		m.pack();
		m.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m.setVisible(true);
	}

}
