Lista Español			Lista Inglés 		Lista Francés

Hola				Hi			Salut
Adios				Goodbye			Adieu
Comida				Food			Nourriture
Dormir				Sleep			Sommeil			
Sol				Sun			Soleir
Agua				Water			L´eau
Desierto			Desert			Désert
Lluvia				Rain			Pluie
Pelota				Ball			Balle
Ordenador			Computer		Ordinateur
Vaso				Glass			Verre